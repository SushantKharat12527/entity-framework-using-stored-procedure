﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EntityFramework_Using_SP.Entities
{
   public class UserEntity
    {
        public int UserId { get; set; }

        public String Firstname { get; set; }

        public String LastName { get; set; }
    }
}
