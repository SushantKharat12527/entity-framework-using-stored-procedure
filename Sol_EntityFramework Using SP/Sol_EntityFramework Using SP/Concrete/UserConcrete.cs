﻿using Sol_EntityFramework_Using_SP.EF;
using Sol_EntityFramework_Using_SP.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EntityFramework_Using_SP.Concrete
{
    public class UserConcrete
    {
        #region Declaration
        private TestdbEntities db = null;
        #endregion

        #region Constructor
        public UserConcrete()
        {
            db = new TestdbEntities();
        }
        #endregion
        public async Task<dynamic> Set(String command,UserEntity userEntityObj,Action<int?,String> ActionStoredProcOut=null)
        {
      

        ObjectParameter Status = null;
            ObjectParameter Message = null;

            try
            {
                return await Task.Run(() => {
                    var SetQuery =
                db
                ?.uspSetUser
                ("Insert"
                , userEntityObj.UserId
                , userEntityObj.Firstname
                , userEntityObj.LastName
                , Status = new ObjectParameter("Status", typeof(Int32))
                , Message = new ObjectParameter("Message", typeof(string))

                         );
                    return (Convert.ToInt32(Status.Value) == 1) ? true : false;

                });
          
            }
            
            catch (Exception)
            {

                throw;
            }


        }
    
    }
    }
    
