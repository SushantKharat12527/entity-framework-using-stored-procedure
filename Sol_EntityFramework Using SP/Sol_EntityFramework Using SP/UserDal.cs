﻿using Sol_EntityFramework_Using_SP.Concrete;
using Sol_EntityFramework_Using_SP.EF;
using Sol_EntityFramework_Using_SP.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EntityFramework_Using_SP
{
    public class UserDal
    {
        #region Declaration
        //private TestdbEntities db = null;
        private UserConcrete userConcreteObj = null;

        #endregion

        #region Constructor
        public UserDal()
        {
            //   db = new TestdbEntities();

            userConcreteObj = new UserConcrete();
        }
        #endregion
        public int? status = null;
        public string message = null;
        #region Public Method
        #region Insert
        public async Task<Boolean> AddAsync(UserEntity userEntityObj)
        {

            //ObjectParameter Status = null;
            //ObjectParameter Message = null;

            //try
            //{
            //    return await Task.Run(() => {
            //        var SetQuery =
            //    db
            //    ?.uspSetUser
            //    ("Insert"
            //    , userEntityObj.UserId
            //    , userEntityObj.Firstname
            //    , userEntityObj.LastName
            //    , Status = new ObjectParameter("Status", typeof(Int32))
            //    , Message = new ObjectParameter("Message", typeof(string))

            //             );
            //        return (Convert.ToInt32(Status.Value) == 1) ? true : false;

            //    });

            //}
            //catch (Exception)
            //{

            //    throw;
            //}
            try
            {
                await userConcreteObj.Set(
              "Insert"
              , userEntityObj,
              (leStatus, leMessage) =>
              {
                  this.status = leStatus;
                  this.message = leMessage;
              });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }



        }
        #endregion

        #region Update
        public async Task<Boolean> UpdateAsync(UserEntity userEntityObj)
        {
            //ObjectParameter Status = null;
            //ObjectParameter Message = null;

            //try
            //{
            //    return await Task.Run(() => {
            //        var SetQuery =
            //    db
            //    ?.uspSetUser
            //    ("Update"
            //    , userEntityObj.UserId
            //    , userEntityObj.Firstname
            //    , userEntityObj.LastName
            //    , Status = new ObjectParameter("Status", typeof(Int32))
            //    , Message = new ObjectParameter("Message", typeof(string))

            //             );
            //        return (Convert.ToInt32(Status.Value) == 1) ? true : false;

            //    });

            //}
            //catch (Exception)
            //{

            //    throw;
            //}

            try
            {
                await userConcreteObj.Set(
              "Update"
              , userEntityObj,
              (leStatus, leMessage) =>
              {
                  this.status = leStatus;
                  this.message = leMessage;
              });

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }





        }
        #endregion

        #region Delete
        public async Task<Boolean> DeleteAsync(UserEntity userEntityObj)
        {
            //    ObjectParameter Status = null;
            //    ObjectParameter Message = null;

            //    try
            //    {
            //        return await Task.Run(() => {
            //            var SetQuery =
            //        db
            //        ?.uspSetUser
            //        ("Delete"
            //        , userEntityObj.UserId
            //        , userEntityObj.Firstname
            //        , userEntityObj.LastName
            //        , Status = new ObjectParameter("Status", typeof(Int32))
            //        , Message = new ObjectParameter("Message", typeof(string))

            //                 );
            //            return (Convert.ToInt32(Status.Value) == 1) ? true : false;

            //        });

            //    }
            //    catch (Exception)
            //    {

            //        throw;
            //    }



            try
            {
                userConcreteObj.Set(
                    "Delete"
                    ,userEntityObj
                    , (leStatus,leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    }
                    );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }


        }
        #endregion
        #endregion


    }
}

